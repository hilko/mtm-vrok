defmodule Vrok.SceneRepo do
  def all() do
    [
      %{operation_id: 0, id: 1, title: "Test", description: "A test!", tags: ["testing"]},
      %{operation_id: 0, id: 2, title: "Test #2", description: "Another one!", tags: ["testing", "second"]},
      %{operation_id: 0, id: 3, title: "Test #3", description: "more", tags: ["more", "third"]},
      %{operation_id: 1, id: 1, title: "Second operation Test #1", description: "second one", tags: ["more", "fourth"]},
      %{operation_id: 1, id: 2, title: "Second operation Test #2", description: "second two", tags: ["more", "whatever"]},
      %{operation_id: 1, id: 3, title: "Second operation Test #3", description: "second three", tags: ["less", "whatever"]},
    ]
  end

  def by_operation(id) do
    all()
    |> Enum.filter(&(Map.get(&1, :operation_id) == id))
  end

  def filter(), do: all()
  def filter(query) do
    scenes = all()
    Vrok.SceneRepo.Filter.run(scenes, query)
  end
end
