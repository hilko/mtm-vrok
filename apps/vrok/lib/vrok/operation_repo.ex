defmodule Vrok.OperationRepo do
  def all() do
    [
      %{
        id: 0,
        title: "Laparoscopische Navelbreuk operatie",
        description: "De navel is het dunste deel van de buikwand. Er kan een breuk ontstaan als er druk op de buikwand komt, bijvoorbeeld door een zwangerschap, toename van gewicht of zwaar lichamelijk werk. Soms was er al een zwakke plek in de buikwand. In de uitstulping van de navelbreuk zit meestal vet. Bij grotere breuken kan er ook een stukje darm in terechtkomen. Een navelbreuk hoeft geen klachten te geven. Als de buikinhoud klem komt te zitten, krijgt u heftige pijn.",
        tags: ["testing"]
      },
      %{
        id: 1,
        title: "Tweede Laparoscopische Navelbreuk operatie",
        description: "Een aangeboren breuk rond de navel is niet ernstig. Klemzittende buikinhoud komt bij deze breuk bijna nooit voor. Meestal herstelt de breuk zich vanzelf voordat een kind 3 jaar is. Gebeurt dat niet, dan kan de specialist een operatie overwegen. Vooral meisjes kunnen later, als ze zwanger zijn, last krijgen van de breuk.",
        tags: ["testing", "second"]
      }
    ]
  end

  def filter(), do: all()

  def filter(query) do
    operations = all()
    Vrok.OperationRepo.Filter.run(operations, query)
  end
end
