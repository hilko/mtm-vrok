defmodule Vrok.Application do
  @moduledoc """
  The Vrok Application Service.

  The vrok system business domain lives in this application.

  Exposes API to clients such as the `VrokWeb` application
  for use in channels, controllers, and elsewhere.
  """
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Supervisor.start_link([
      supervisor(Vrok.Repo, []),
    ], strategy: :one_for_one, name: Vrok.Supervisor)
  end
end
