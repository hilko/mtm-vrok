use Mix.Config

# Configure your database
config :vrok, Vrok.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "vrok_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
