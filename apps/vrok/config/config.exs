use Mix.Config

config :vrok, ecto_repos: [Vrok.Repo]

import_config "#{Mix.env}.exs"
