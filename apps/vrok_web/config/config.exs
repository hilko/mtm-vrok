# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :vrok_web,
  namespace: VrokWeb,
  ecto_repos: [Vrok.Repo]

# Configures the endpoint
config :vrok_web, VrokWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "6EoQVQiJ7zY2eTDlHEojxH6MBC1L/uM8pa0TKpNw327G/VC1rx6mSiJME4cxYL7P",
  render_errors: [view: VrokWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: VrokWeb.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :vrok_web, :generators,
  context_app: :vrok

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
