import React from 'react';
import ReactDOM from 'react-dom';

import {niceTimestamp} from '../../helpers';

function currentProgress(duration, position) {
  if (duration <= 0 || position <= 0) return 0;

  const progress =  (position / duration) * 100
  return Math.round(progress * 100) / 100
}

export class Progress extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      position: 0,
      duration: 0,
    }
  }
  componentDidMount() {

    window.controllerChannel.on("video_data", ({position, duration}) => {
      if (position < 0) {position = 0}
      this.setState({position, duration})
    });
  }
  render() {
    const duration = this.state.duration;
    const position = this.state.position;
    const progress = currentProgress(duration, position);
    // const progress = `${position} / ${duration}`
    return (
      <div className="cProgress">
        <div className="bar">
          <div className="bar-filling" style={{width: `${progress}%`}}></div>
          <div className="bar-text">{niceTimestamp(position) + ' / ' + niceTimestamp(duration)}</div>
        </div>
      </div>
    )
  }
}