const primers = [
  {
    from: 0,
    to: 2,
    type: 'text',
    value: "Deze virtuele omgeving toont een arteriale monochromie operatie. U kunt op elk moment pauzeren door de pauze-knop of spatiebalk in te drukken."
  },
  {
    from: 2,
    to: 5,
    type: 'text',
    value: "Af en toe zal de video pauzeren en wordt u gevraagd om een multiple-choice vraag te beantwoorden."
  },
  {
    from: 5,
    to: 10,
    type: 'text',
    value: "Nullam quis risus eget urna mollis ornare vel eu leo. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Etiam porta sem malesuada magna mollis euismod. Maecenas faucibus mollis interdum.",
  },
  {
    from: 10,
    to: 15,
    type: 'qanda',
    question: "Dit is de eerste vraag. Welke kleur schoenen draagt Wouter?",
    answers: [
      "Blauw",
      "Groen",
      "Oranje",
      "Alle bovenstaande kleuren"
    ],
    correctAnswer: 3,
  },
  {
    from: 15,
    to: 17,
    type: 'text',
    value: "But we continue on with the text and playback"
  },

  // {
  //   from: 6,
  //   to: 9,
  //   type: 'text',
  //   value: "Even video, if we want!"
  // },
  // {
  //   from: 8,
  //   to: 15,
  //   type: 'video',
  //   src: "https://www.youtube.com/embed/dQw4w9WgXcQ", 
  // },
  {
    from: 17,
    to: 19,
    type: 'text',
    value: "and say something interesting"
  },
  {
    from: 19,
    to: 22,
    type: 'qanda',
    value: "when finally we pause and make the user answer a question!",
    pause: true,
  },
  {
    from: 22,
    to: 27,
    type: 'text',
    value: "Cras mattis consectetur purus sit amet fermentum. Nulla vitae elit libero, a pharetra augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur et."
  },
]

export {primers}