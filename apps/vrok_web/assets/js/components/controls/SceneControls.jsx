import React from 'react';
import ReactDOM from 'react-dom';
import Shake from 'shake.js';

function playPause() {
  window.controllerChannel.push("play_pause", {});
}

function rewind() {
  window.controllerChannel.push("rewind", {});
}

function currentProgress(duration, position) {
  const progress =  (position / duration) * 100
  return `${Math.round(progress)}%`
}

export class SceneControls extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      paused: true,
      sceneFilename: null,
    }
    this.onPlayPauseClick = this.onPlayPauseClick.bind(this);
  }
  componentDidMount() {
    document.addEventListener('keypress', (e) => {
      console.log('keypress: ' + e.which);
      if (e.which == 32) {
        this.onPlayPauseClick(e);
      }
    })
  }
  onRewindClick(e) {
    e.preventDefault();
    rewind();
  }
  onPlayPauseClick(e) {
    e.preventDefault();
    this.setState({paused: !this.state.paused})
    playPause();
  }
  onFlipClick(e) {
    e.preventDefault();
    window.controllerChannel.push("flip_screen", {path: window.location.pathname});
  }
  onPrevClick(e) {
    e.preventDefault();
    alert('clicked previous');
  }
  onNextClick(e) {
    e.preventDefault();
    alert('clicked next');
  }
  render() {
    const duration = this.state.duration;
    const position = this.state.position;
    const playpauseStatusIcon = this.state.paused ? "play-icon" : "pause-icon";
    return (
      <div className="cSceneControls">
        <ul>
          <li><a className="rewind" href="#" onClick={this.onRewindClick}></a></li>
          <li><a className={playpauseStatusIcon} href="#" onClick={this.onPlayPauseClick}></a></li>
          <li><a className="flip" href="#" onClick={this.onFlipClick}></a></li>
        </ul>
      </div>
    )
  }
}