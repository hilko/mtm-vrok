import React from 'react';
import ReactDOM from 'react-dom';

function operationItem({id, title}, onOperationItemClick) {
  return <li key={id}><a href="#" onClick={(e) => onOperationItemClick(e, id)}>{title}</a></li>
}

class OperationList extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const operations = this.props.operations;
    const onOperationItemClick = this.props.onOperationItemClick;
    return (
      <div className="cOperationList">
        <ul>
          {
            operations.map((item) => operationItem(item, onOperationItemClick))
          }
        </ul>
      </div>
    )
  }
}

function sceneItem(props, onSceneItemClick) {
  const id = props.id;
  const title = props.title;
  const filename = props.filename;
  return <li key={id}><a href="#" onClick={(e) => onSceneItemClick(e, id, filename)}>{title} - {filename}</a></li>
}

class SceneList extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const operationId = this.props.operationId;
    const operations = this.props.operations;
    const onSceneItemClick = this.props.onSceneItemClick;

    const scenes = operations[operationId].scenes;

    return (
      <div className="cSceneList">
        <ul>
          {
            scenes.map((item) => sceneItem(item, onSceneItemClick))
          }
        </ul>
      </div>
    )
  }
}

export class OperationSelection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      operationId: null,
    }
    this.handleOperationItemClick = this.handleOperationItemClick.bind(this);
  }
  handleOperationItemClick(e, id) {
    e.preventDefault();
    this.setState({
      operationId: id 
    })
  }
  render(props) {
    const operations = this.props.operations;
    const onSceneItemClick = this.props.onSceneItemClick;
    const operationId = this.state.operationId;
    const sceneId = this.state.sceneId;

    if (!operations && operations.length <= 0) {
      return (<div>select operation</div>)
    }
    if (!sceneId && !operationId) {
      return <OperationList operations={operations} onOperationItemClick={this.handleOperationItemClick} />
    }
    if (operationId && !sceneId) {
      return <SceneList operations={operations} operationId={operationId} onSceneItemClick={onSceneItemClick} />
    }
  }
}
