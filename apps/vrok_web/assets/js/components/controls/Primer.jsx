import React from 'react';
import ReactDOM from 'react-dom';
import {primers} from './primers'

import {niceTimestamp} from '../../helpers';


function Qanda(props, onClick) {
  const {question, from, position, answers, correctAnswer} = props;
  const hidden =  position < from //|| position > to
  const classes = ['item', 'qanda', hidden ? "hidden" : ""].join(" ")

  return (
    <div className={classes}>
      <div className="question">
        {question}
      </div>
      {answers && answers.map((answer, index) => <Answer key={index} index={index} text={answer} correctAnswerId={correctAnswer}/>)}
    </div>
  )
}

function Answer({index, text, correctAnswerId}) {
  return <div className="answer" onClick={(e) => clickAnswer(e, index, correctAnswerId)} >{text}</div>
}

function clickAnswer(e, index, correctAnswerId) {
  controllerChannel.push("select_answer", {answerId: index, correctAnswerId})
}

function Item(props) {
  const {from, to, type, value, position, src, onClick, pause} = props;
  const hidden =  position < from //|| position > to
  const isActive = position > from && position < to
  let elem;

  if (type == 'qanda') {
    elem = <Qanda {...props} />
  }

  if (type == 'text') {
    elem = <div onClick={onClick} className={`item ${type} ${hidden ? "hidden" : ""}`}><span className="timestamp">{niceTimestamp(from)}</span>|{value}</div>
  }

  // FIXME: figure out how to do this when position is not always very granular
  // if (pause == true && position > from && position < from + 3) {
  //   window.controllerChannel.push("play_pause", {});
  // }

  return (<a href="#" className={isActive ? "active" : ""}>{elem}</a>)
}


function handlePrimerClick(e, from) {
  e.preventDefault();
  window.controllerChannel.push("scrub_to", ({position: from}))
}


export class Primer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      position: 0,
    }
  }
  componentDidMount() {

    window.controllerChannel.on("video_data", ({position, duration}) => {
      this.setState({position})
    });

    window.controllerChannel.on("select_answer", (res) => {
      const answerId = res.answer_id;
      const correctAnswerId = res.correct_answer_id;

      if (answerId == correctAnswerId) {
        window.alert('Correct! The video will now continue.')
        window.controllerChannel.push("play_pause", {})
        this.setState({playing: true})
      }
    });

  }
  render() {
    const position = this.state.position;
    return (
      <div className="cPrimer">
        {primers.map((primer, index) => <Item key={index} {...primer} position={position} onClick={(e) => handlePrimerClick(e, primer.from)} />)}
      </div>
    )
  }
}