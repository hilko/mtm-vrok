import React from 'react';
import ReactDOM from 'react-dom';
import {Message} from './Message';

import {SceneControls} from './controls/SceneControls';
import {Progress} from './controls/Progress';
import {Primer} from './controls/Primer';

function ConnectionStatus({viewerConnected}) {
  const viewerUrl = window.location.origin + "/viewer";
  if (!viewerConnected) {
    return (
      <Message type="alert">Please open <a href={viewerUrl}>{viewerUrl}</a> on another device, browser or tab.</Message>
    )
  } else {
    return <Message>Viewer is connected</Message>
  }

}

export class Controls extends React.Component {
  constructor(props) {
    console.log('constructing Controls component')
    super(props);
    this.state = {
      operationId: window.operationId,
      sceneId: window.sceneId,
      viewerConnected: false,
    }

  }
  componentDidMount() {
    // window.userChannel.push("controls:ready", {operation_id: this.state.operationId, scene_id: this.state.sceneId});

    // window.userChannel.on("controls:flip_controls", ({path}) => {
    //   window.location.pathname = path;
    // });

    window.controllerChannel.on("viewer_connected", () => {
      this.setState({viewerConnected: true})
      const {operationId, sceneId} = this.state;
      controllerChannel.push("scene_data", {operationId, sceneId})
    });

    controllerChannel.push("controller_connected", {})

    // connector = setInterval(() => {
    //   window.userChannel.push("controls:state");
    // }, 1000)
  }
  render() {
    const operationId = this.state.operationId;
    const sceneId = this.state.sceneId;
    const viewerConnected = this.state.viewerConnected;
    return (
      <div>
        <ConnectionStatus viewerConnected={viewerConnected} />
        <div className="page">
          <div className="sticky">
            <SceneControls />
            <Progress />
          </div>
          <Primer />
        </div>
      </div>
    )
  }
}