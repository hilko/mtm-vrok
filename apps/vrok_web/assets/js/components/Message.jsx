import React from 'react';
import ReactDOM from 'react-dom';

export function Message(props) {
  return <div className={`cMessage ${props.type}`}>{props.children}</div>
}
