import React from 'react';
import ReactDOM from 'react-dom';


export class Search extends React.Component {
  constructor(props) {
    console.log('constructing Search component')
    super(props);
    this.state = {
      query: "",
      results: {
        scenes: [],
        operations: [],
      }
    }
    this.handleChange = this.handleChange.bind(this);

  }
  componentDidMount() {
    window.controllerChannel.on("search_results", ({scenes, operations}) => {
      this.setState({results: {scenes, operations}})
    });
  }
  componentWillUnmount() {
  }
  handleChange(event) {
    window.controllerChannel.push("search", {query: event.target.value})
    this.setState({query: event.target.value});
  }
  render() {
    const operations = this.state.results.operations || [];
    const scenes = this.state.results.scenes || [];
    const query = this.state.query || ""
    const hasResults = scenes.length > 0 || operations.length > 0 ? 'hasResults' : ''
    const cSearchClasses = ['cSearch ', hasResults].join("");
    const cSearchResultsClasses = ['cSearchResults ', hasResults].join("");
    return (
      <div className={cSearchClasses}>
        <input placeholder="search Operations or Scenes" value={query} onChange={this.handleChange} />
        <div className={cSearchResultsClasses}>
          <div className="operationResults">
            {operations.length > 0 && <div className="title">Operaties</div>}
            {operations.length > 0 && operations.map(operation => <Operation key={operation.id} {...operation} />)}
          </div>
          <div className="sceneResults">
            {scenes.length > 0 && <div className="title">Scenes</div>}
            {scenes.length > 0 && scenes.map((scene, index) => <Scene key={index} {...scene} />)}
          </div>
        </div>
      </div>
    )
  }
}

function Operation(props) {
  const {title, tags, id, description} = props;
  return (
    <a href={`/operations/${id}/scenes`}><div className="item operation">
      {title}
    </div></a>
  )
}

function Scene(props) {
  const {title, tags, operation_id, id, description} = props;
  return (
    <a href={`/operations/${operation_id}/scenes/${id}`}><div className="item operation">{title}</div></a>
  )
}
