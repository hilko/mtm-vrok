import React from 'react';
import ReactDOM from 'react-dom';
import {Message} from './Message';


export class Viewer extends React.Component {
  constructor(props) {
    console.log('constructing Scene component')
    super(props);
    this.state = {
      playing: false,
      connected: false,
      operationId: null,
      sceneId: null,
      position: 0,
      duration: null,
    }
  }
  componentDidMount() {
    // window.controllerChannel.on("controls:flip_screen", ({path}) => {
    //   console.log(`flipping this screen to ${path}`);
    //   window.controllerChannel.push("controls:flip_controls", {path: window.location.pathname});
    //   window.location.pathname = path;
    // });

    window.controllerChannel.on("scrub_to", ({position}) => {
      console.log("scrubbing");
      const video = ReactDOM.findDOMNode(this.refs.activeScene);
      video.currentTime = position;
    }); 

    window.controllerChannel.on("controller_connected", () => {
      window.location.reload();
    });

    window.controllerChannel.on("scene_data", ({operation_id, scene_id}) => {
      const video = ReactDOM.findDOMNode(this.refs.activeScene);
      video.currentTime = 0;
      this.setState({operationId: operation_id , sceneId: scene_id})
    }); 

    window.controllerChannel.on("play_pause", () => {
      console.log("play/pause clicked!");
      const video = ReactDOM.findDOMNode(this.refs.activeScene);
      if (!this.state.playing) {
        video.play();
        this.setState({playing: true})
        if (video) {
          this.setState({duration: video.duration})
          window.sceneIntervaller = setInterval(() => {
            const duration = video.duration;
            const position = video.currentTime;
            window.controllerChannel.push("video_data", {position, duration});
          }, 1000)
        }

      } else {
        video.pause();
        this.setState({playing: false})
        window.clearInterval(window.sceneIntervaller);
      }
    });

    // window.controllerChannel.on("controls:state", (controls_state) => {
    //   this.setState({
    //     operationId: controls_state.operation_id,
    //     sceneId: controls_state.scene_id,
    //   })
    window.controllerChannel.push("viewer_connected");
    this.setState({connected: true})
  }
  render() {
    const connected = this.state.connected;
    const operationId = this.state.operationId;
    const sceneId = this.state.sceneId;
    const playing = this.state.playing;

    if (!connected) {return <div className="cViewer waiting">Waiting for controls...</div>}

    const filename = 'o' + operationId + 's' + sceneId; 
    const videoSrc = `/videos/${filename}.mp4`

    return (
      <div className="cViewer">
        { playing ? <Message>Playing...</Message> : "" }
        { !playing ? <Message type="alert">Paused</Message> : "" }
        <a-scene>
          <a-assets>
            <img id="hotspotimage" src="/images/phoenix.png" />
            <video id="activeScene" 
              ref="activeScene"
              src={videoSrc}
              muted
            ></video>
          </a-assets>
          <a-videosphere id="backdrop" src="#activeScene"></a-videosphere>
          <a-cursor color="#4CC3D9" fuse="true" timeout="10"></a-cursor>
          <a-camera>
            <a-cursor></a-cursor>
          </a-camera>
        </a-scene>
      </div>
    )
  }
}
