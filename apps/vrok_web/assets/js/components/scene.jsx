import React from 'react';
import ReactDOM from 'react-dom';

export class Scene extends React.Component {
  constructor(props) {
    console.log('constructing Scene component')
    super(props);
    this.state = {
      playing: false
    }
  }
  componentDidMount() {
    let intervaller;
    window.userChannel.on("scene:flip", ({path}) => {
      console.log(`flipping this screen to ${path}`);
      window.userChannel.push("controls:flip", {path: window.location.pathname});
      window.location.pathname = path;
    });

    window.userChannel.on("scene:play_pause", () => {
      console.log("play/pause clicked!");
      const video = ReactDOM.findDOMNode(this.refs.activeScene);
      if (!this.state.playing) {
        video.play();
        this.setState({playing: true})
      } else {
        video.pause();
        this.setState({playing: false})
      }
    });
    window.userChannel.push("controls:initialize", {sceneId: this.props.sceneId, operationId: this.props.operationId});
    const video = ReactDOM.findDOMNode(this.refs.activeScene);
    if (video) {
      window.sceneIntervaller = setInterval(() => {
        window.userChannel.push("scene:status", {duration: video.duration, position: video.currentTime});
      }, 1000)
    }

  }
  componentWillUnmount() {
    window.userChannel.off("scene:flip")
    window.userChannel.off("scene:play_pause")
    window.clearInterval(window.sceneIntervaller);
  }
  render() {
    const sceneId = this.props.sceneId;
    const operationId = this.props.operationId;
    const videoSrc = `/videos/${sceneId}.mp4`
    console.log('rendering scene');
    if (!sceneId) {return <div>no scene id</div>}
    return (
      <a-scene>
        <a-assets>
          <img id="hotspotimage" src="/images/phoenix.png" />
          <video id="activeScene" 
            ref="activeScene"
            src={videoSrc}
            muted
          ></video>
        </a-assets>
        <a-videosphere id="backdrop" src="#activeScene"></a-videosphere>
        <a-cursor color="#4CC3D9" fuse="true" timeout="10"></a-cursor>
        <a-camera>
          <a-cursor></a-cursor>
        </a-camera>
      </a-scene>
    )
  }
}
