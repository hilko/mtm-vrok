export function niceTimestamp(from) {
  return new Date(from * 1000).toISOString().substr(14, 5);
}
