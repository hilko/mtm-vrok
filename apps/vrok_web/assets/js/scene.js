// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".

// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies

// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html";

require('aframe');
require('aframe-sprite-component');

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

import socket from "./socket";

// import * as todoChannel from "./channels/todoChannel";
import React from 'react';
import ReactDOM from 'react-dom';
import {Scene} from "./components/scene";

socket.connect();
if (window.userId) {
  const sceneContainer = document.querySelector('.cScene');
  const operationId = window.initialState.operationId;
  const sceneId = window.initialState.sceneId;

  if (sceneContainer) {
    ReactDOM.render(<Scene operationId={operationId} sceneId={sceneId} />, sceneContainer)
  }
}

