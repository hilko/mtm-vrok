import socket from "../socket";
let userChannel = {};

function initialize(socket, userId) {
  console.log('initializing user-channel');
  let userChannel = socket.channel("user:" + userId);

   userChannel.join()
     .receive("ok", resp => console.log("joined the user channel", resp) )
     .receive("error", reason => console.log("join failed", reason) )

  return userChannel;
}

userChannel.initialize = initialize;

export {userChannel}