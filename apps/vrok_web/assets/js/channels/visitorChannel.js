import socket from "../socket";
let visitorChannel = {};

function initialize(socket) {
  console.log('initializing visitor-channel');
  let visitorChannel = socket.channel("visitor:lobby");

   visitorChannel.join()
     .receive("ok", resp => console.log("joined the visitor channel", resp) )
     .receive("error", reason => console.log("join failed", reason) )

  visitorChannel.on("meaning_of_life?", ({answer}) => console.log("meaning of life?", answer) )
  return visitorChannel;
}

visitorChannel.initialize = initialize;

export {visitorChannel}