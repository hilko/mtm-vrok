import socket from "../socket";
let controllerChannel = {};

function initialize(socket, userId) {
  console.log('initializing controller-channel');
  let controllerChannel = socket.channel("controller:" + userId);

   controllerChannel.join()
     .receive("ok", resp => console.log("joined the controller channel", resp) )
     .receive("error", reason => console.log("join failed", reason) )

  return controllerChannel;
}

controllerChannel.initialize = initialize;

export {controllerChannel}