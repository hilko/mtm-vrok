defmodule VrokWeb.ControllerChannel do
  use VrokWeb, :channel
  require Logger

  def join("controller:" <> user_id, _params, socket) do
    case socket.assigns[:user_id] == String.to_integer(user_id) do
      true -> 
        send self(), :after_join
        {:ok, socket}
      false -> 
        {:error, "connection error"}
    end
  end

  # example of handling user-specific messages

  def handle_in("viewer_connected", _params, socket) do
    broadcast!(socket, "viewer_connected", %{})
    {:noreply, socket}
  end

  def handle_in("controller_connected", _params, socket) do
    broadcast!(socket, "controller_connected", %{})
    {:noreply, socket}
  end

  def handle_in("scene_data", params, socket) do
    broadcast!(socket, "scene_data", %{operation_id: params["operationId"], scene_id: params["sceneId"]})

    {:noreply, socket}
  end

  def handle_in("play_pause", _params, socket) do
    broadcast!(socket, "play_pause", %{})
    {:noreply, socket}
  end

  def handle_in("scrub_to", params, socket) do
    broadcast!(socket, "scrub_to", %{position: params["position"]})
    {:noreply, socket}
  end

  def handle_in("video_data", params, socket) do
    video_state = %{
      duration: params["duration"],
      position: params["position"],
    }
    broadcast!(socket, "video_data", video_state)
    # {:noreply, assign(socket, :viewer_state, viewer_state)}
    {:noreply, socket}
  end

  def handle_in("select_answer", params, socket) do
    broadcast!(socket, "select_answer", %{answer_id: params["answerId"], correct_answer_id: params["correctAnswerId"]})
    {:noreply, socket}
  end

  def handle_in("search", params, socket) do
    query = params["query"]
    scenes = Vrok.SceneRepo.filter(query)
    operations = Vrok.OperationRepo.filter(query)

    broadcast!(socket, "search_results", %{scenes: scenes, operations: operations})
    {:noreply, socket}
  end

  # def handle_in("controls:scene_selected", %{"id" => id, "filename" => filename}, socket) do
  #   socket = assign(socket, :current_scene, id)
  #   socket = assign(socket, :filename, filename)

  #   broadcast!(socket, "state:current_scene", %{
  #     current_scene: socket.assigns[:current_scene], 
  #     filename: socket.assigns[:filename]
  #   })
  #   {:noreply, socket}
  # end

  # def handle_in("viewer:ready", params, socket) do
  #   broadcast!(socket, "viewer:ready", socket.assigns[:controls_state])
  #   {:noreply, socket}
  # end


  # def handle_in("viewer:video_state", params, socket) do
  #   duration = params["duration"]
  #   position = params["position"]
  #   broadcast!(socket, "viewer:video_state", %{position: position, duration: duration})
  #   {:noreply, socket}
  # end

  # def handle_in("viewer:state", _params, socket) do
  #   push(socket, "viewer:state", %{viewer_state: socket.assigns[:viewer_state]})
  #   {:noreply, socket}
  # end

  # def handle_in("controls:initialize", %{"operationId" => operation_id, "sceneId" => scene_id}, socket) do
  #   broadcast!(socket, "controls:initialize", %{operationId: operation_id, sceneId: scene_id})
  #   {:noreply, socket}
  # end

  # def handle_in("scene:status", %{"duration" => duration, "position" => position}, socket) do
  #   broadcast!(socket, "controls:status", %{duration: duration, position: position})
  #   {:noreply, socket}
  # end

  # def handle_in("flip", %{"path" => path}, socket) do
  #   broadcast!(socket, "flip", %{path: path})
  #   {:noreply, socket}
  # end

  def handle_in(_topic, _params, socket) do
    require Logger
    Logger.error("no matching function clause for incoming user channel message")
    {:noreply, socket}
  end

  def handle_info(:after_join, socket) do 
    # broadcast!(socket, "controller_connected", %{})
    {:noreply, socket} 
  end

end
