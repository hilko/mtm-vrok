defmodule VrokWeb.VisitorChannel do 
  use VrokWeb, :channel

  def join("visitor:lobby", _params, socket) do
    {:ok, socket}
  end 

    def handle_in("meaning_of_life?", _params, socket) do 

    # todos = Repo.all(user_todos(user))
    push socket, "meaning_of_life?", %{answer: 42}
    {:noreply, socket} 
  end

end