defmodule VrokWeb.UserChannel do
  use VrokWeb, :channel
  require Logger

  def join("user:" <> user_id, _params, socket) do
    case socket.assigns[:user_id] == String.to_integer(user_id) do
      true ->
        # send(self(), :after_join)
        {:ok, socket}

      false ->
        {:error, "connection error"}
    end
  end

  # example of handling user-specific messages

  # def handle_in("controls:ready", params, socket) do
  #   controls_state = %{
  #     operation_id: params["operation_id"],
  #     scene_id: params["scene_id"]
  #   }
  #   {:noreply, assign(socket, :controls_state, controls_state)}
  # end

  # def handle_in("controls:state", _params, socket) do
  #   broadcast!(socket, "controls:state", socket.assigns[:controls_state])
  #   {:noreply, socket}
  # end

  # def handle_in("controls:rewind", _params, socket) do
  #   broadcast!(socket, "controls:rewind", %{})
  #   {:noreply, socket}
  # end

  # def handle_in("controls:play_pause", params, socket) do
  #   broadcast!(socket, "controls:play_pause", %{})
  #   {:noreply, socket}
  # end

  # def handle_in("controls:scene_selected", %{"id" => id, "filename" => filename}, socket) do
  #   socket = assign(socket, :current_scene, id)
  #   socket = assign(socket, :filename, filename)

  #   broadcast!(socket, "state:current_scene", %{
  #     current_scene: socket.assigns[:current_scene], 
  #     filename: socket.assigns[:filename]
  #   })
  #   {:noreply, socket}
  # end

  # def handle_in("viewer:ready", params, socket) do
  #   broadcast!(socket, "controls:state", socket.assigns[:controls_state])
  #   {:noreply, socket}
  # end

  # def handle_in("viewer:state", params, socket) do
  #   viewer_state = %{
  #     duration: params["duration"],
  #     position: params["position"],
  #     playing: params["playing"]
  #   }
  #   broadcast!(socket, "viewer:state", viewer_state)
  #   # {:noreply, assign(socket, :viewer_state, viewer_state)}
  #   {:noreply, socket}
  # end

  # def handle_in("viewer:video_state", params, socket) do
  #   duration = params["duration"]
  #   position = params["position"]
  #   broadcast!(socket, "viewer:video_state", %{position: position, duration: duration})
  #   {:noreply, socket}
  # end

  # # def handle_in("viewer:state", _params, socket) do
  # #   push(socket, "viewer:state", %{viewer_state: socket.assigns[:viewer_state]})
  # #   {:noreply, socket}
  # # end

  # # def handle_in("controls:initialize", %{"operationId" => operation_id, "sceneId" => scene_id}, socket) do
  # #   broadcast!(socket, "controls:initialize", %{operationId: operation_id, sceneId: scene_id})
  # #   {:noreply, socket}
  # # end

  # # def handle_in("scene:status", %{"duration" => duration, "position" => position}, socket) do
  # #   broadcast!(socket, "controls:status", %{duration: duration, position: position})
  # #   {:noreply, socket}
  # # end

  # def handle_in("controls:flip_screen", %{"path" => path}, socket) do
  #   broadcast!(socket, "controls:flip_screen", %{path: path})
  #   {:noreply, socket}
  # end

  # def handle_in("controls:flip_controls", %{"path" => path}, socket) do
  #   broadcast!(socket, "controls:flip_controls", %{path: path})
  #   {:noreply, socket}
  # end

  # # def handle_in("scene:flip", %{"path" => path}, socket) do
  # #   broadcast!(socket, "scene:flip", %{path: path})
  # #   {:noreply, socket}
  # # end

  # # defp user_todos(user) do
  # #   Ecto.assoc(user, :todos)
  # # end

  def handle_in(_topic, _params, socket) do
    require Logger
    Logger.error("no matching function clause for incoming user channel message")
    {:noreply, socket}
  end

  # def handle_info(:after_join, socket) do 
  #   broadcast!(socket, "controls:initialize", %{"operationId" => nil, "sceneId" => nil})
  #   {:noreply, socket} 
  # end
end
