defmodule VrokWeb.SceneController do
  use VrokWeb, :controller
  require Logger

  def index(conn, params) do
    operation_id = String.to_integer(params["operation_id"])
    scenes = Vrok.SceneRepo.by_operation(operation_id)

    render conn, "index.html", scenes: scenes
  end

  def show(conn, %{"operation_id" => operation_id, "id" => scene_id}) do
    render conn, "show.html", %{operation_id: operation_id, scene_id: scene_id}
  end
end
