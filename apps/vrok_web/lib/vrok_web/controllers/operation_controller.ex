defmodule VrokWeb.OperationController do
  use VrokWeb, :controller
  require Logger

  def index(conn, _params) do
    operations = Vrok.OperationRepo.all()

    render conn, "index.html", operations: operations
  end
end
