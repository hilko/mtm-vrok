defmodule VrokWeb.ViewerController do
  use VrokWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
