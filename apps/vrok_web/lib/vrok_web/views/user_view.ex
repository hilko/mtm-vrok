defmodule VrokWeb.UserView do
  use VrokWeb, :view
  alias Vrok.User

  def first_name(%User{name: name}) do
    name
    |> String.split(" ")
    |> Enum.at(0)
  end
end
