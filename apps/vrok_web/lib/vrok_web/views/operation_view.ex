defmodule VrokWeb.OperationView do
  use VrokWeb, :view

  def nice_tags(tags), do: Enum.join(tags, ", ")
end
