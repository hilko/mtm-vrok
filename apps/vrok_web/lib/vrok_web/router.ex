defmodule VrokWeb.Router do
  use VrokWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(Vrok.Auth, repo: Vrok.Repo)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", VrokWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/", PageController, :index)
    get("/viewer", ViewerController, :index)
    resources("/users", UserController, only: [:index, :show, :new, :create])
    resources("/sessions", SessionController, only: [:new, :create, :delete])

    resources "/operations", OperationController, only: [:index, :new, :create, :delete] do
      resources("/scenes", SceneController, only: [:index, :show])
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", VrokWeb do
  #   pipe_through :api
  # end
end
