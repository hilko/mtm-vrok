defmodule Vrok.SceneRepo.Filter do
  import String, only: [downcase: 1]
  def run(scenes, query \\ "") when is_binary(query) do

    Enum.filter(scenes, fn (scene) -> contains_query(scene, query) end)
  end

  defp contains_query(scene, ""), do: false

  defp contains_query(scene, query) do
    query = String.downcase(query)
    with  result <- String.downcase(scene.title) =~ query,
          result <- [result, String.downcase(scene.description) =~ query],
          result <- [result, tags_contain(scene.tags, query)]
    do
      result
      |> List.flatten([])
      |> Enum.member?(true)
    end
  end

  def tags_contain(tags, query) do
    tags
    |> Enum.map(fn (tag) -> downcase(tag) =~ query end)
    |> Enum.member?(true)
  end

end