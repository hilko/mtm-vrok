defmodule Vrok.OperationRepo.Filter do
  import String, only: [downcase: 1]
  def run(operations, query \\ "") when is_binary(query) do

    Enum.filter(operations, fn (operation) -> contains_query(operation, query) end)
  end

  defp contains_query(operation, ""), do: false

  defp contains_query(operation, query) do
    query = String.downcase(query)
    with  result <- String.downcase(operation.title) =~ query,
          result <- [result, String.downcase(operation.description) =~ query],
          result <- [result, tags_contain(operation.tags, query)]
    do
      result
      |> IO.inspect
      |> List.flatten([])
      |> Enum.member?(true)
    end
  end

  def tags_contain(tags, query) do
    tags
    |> Enum.map(fn (tag) -> downcase(tag) =~ query end)
    |> Enum.member?(true)
  end

end