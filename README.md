# AuthWithChannels.Umbrella

Starter Kit for a complete but basic authentication implementation, with authenticated *and* anonymous channels all set up.

## Implementation

- Authentication
    + Following the Programming Phoenix book. Uses encrypted cookie for user session, and tokens for user channel.
- Routes
    + home
    + registration
    + login
    + logout
    + user:index
    + user:show
- Channels
    + UserChannel user-specific stuff. Authenticated using 